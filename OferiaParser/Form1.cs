﻿using CsQuery;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OferiaParser
{
    public partial class Form1 : Form
    {
        public List<OferiaZl> zl = new List<OferiaZl>();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DownloadPageAsync();
        }

        async void DownloadPageAsync()
        {
            // ... Target page.
            string page = "http://oferia.pl/zlecenia";

            // ... Use HttpClient.
            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(page))
            using (HttpContent content = response.Content)
            {
                // ... Read the string.
                string result = await content.ReadAsStringAsync();

                CQ dom = result;
                
                List<IDomObject> listing_items = dom[".listing-item"].ToList();
                foreach(var item in listing_items)
                {
                     zl.Add(CreateZlecenie(item));
                }
                foreach (var zlecenie in zl)
                {
                    richTextBox1.Text += zlecenie.Link;
                    richTextBox1.Text += zlecenie.Title;
                    richTextBox1.Text += zlecenie.MiddlePrice;
                    richTextBox1.Text += zlecenie.Description;
                    richTextBox1.Text += zlecenie.MiddlePrice;
                    richTextBox1.Text += zlecenie.Place;
                    richTextBox1.Text += zlecenie.From;
                    richTextBox1.Text += zlecenie.ToEnd;
                    richTextBox1.Text += " \n \n \n ";
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public OferiaZl CreateZlecenie(IDomObject item)
        {
            OferiaZl z = new OferiaZl();
            var dom_item = CQ.Create(item);
            z.Title = System.Net.WebUtility.HtmlDecode(dom_item[".listing-order-name a "].Html());
            z.Link = "http://oferia.pl" + dom_item[".listing-order-name a"].Attr("href");
            z.Description = System.Net.WebUtility.HtmlDecode(dom_item[".listing-order-content"].Html());
            z.MiddlePrice = dom_item[".price"].Text();
            z.Place = System.Net.WebUtility.HtmlDecode(dom_item[".listing-order-location a"].Text());
            z.From = System.Net.WebUtility.HtmlDecode(dom_item[".listing-order-date"].Text());
            z.ToEnd = System.Net.WebUtility.HtmlDecode(dom_item[".listing-order-dateend"].Text());
            return z;
        }
    }


    public class OferiaZl
    {
        public  string Title;
        public string Link;
        public string Description;
        public string MiddlePrice;
        public string From;
        public string Place;
        public string ToEnd;
    }
}
